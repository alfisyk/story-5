from django.db import models

# Create your models here.
class Sched(models.Model):
    nama = models.CharField(max_length = 50, default="")
    tempat = models.CharField(max_length = 50, default="")
    tanggal = models.DateField( default="")
    waktu = models.TimeField( default="")
    kategori = models.CharField(max_length = 50, default="")

