from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import FormJadwal
from .models import Sched
from datetime import datetime


# Create your views here.

def index(request):
    return render(request,'index.html', {})

def index2(request):
    return render(request,'index2.html', {})

def indexs(request):
    jadwalformulir= FormJadwal()
    argument= {
        'generated_html': jadwalformulir
    }
    return render(request,'isi.html',argument)


def daftarjadwal(request):

    if request.method=="POST":
        jdwlpost= FormJadwal(request.POST)
        if 'id' in request.POST:
            Sched.objects.get(id=request.POST['id']).delete()
            return redirect('/daftarjadwal/')

        if jdwlpost.is_valid():
            jadwal= Sched(
                nama = jdwlpost.data['nama'],
                tempat = jdwlpost.data['tempat'],
                kategori = jdwlpost.data['kategori'],
                tanggal = jdwlpost.data['tanggal_year'] + "-" + jdwlpost.data['tanggal_month'] + "-" + jdwlpost.data['tanggal_day'],
                waktu = jdwlpost.data['waktu'],
                )
            jadwal.save()
            return redirect('/daftarjadwal/')    
    else:
        jdwlpost = FormJadwal()

    schedule1= Sched.objects.all().values()
    argument={
        'schedules_active' : 'active',
        'context' : {'now': datetime.now()},
        'jadwalbos': schedule1,
        'form' : jdwlpost,
    }
    return render(request, 'hasil.html', argument)