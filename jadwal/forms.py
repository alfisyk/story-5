from django import forms
from jadwal import models

rentangtahun = [x for x in range(2019, 2030)]

class FormJadwal(forms.Form):
    nama = forms.CharField(
        label = 'Nama Kegiatan', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Nama Kegiatan"}),
    )
    tempat = forms.CharField(
        label = 'Tempat', max_length = 50, required = True,
        widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Tempat Kegiatan"}),
    )
    tanggal = forms.DateField(
        label = 'Tanggal', required = True,
        widget = forms.SelectDateWidget(years = rentangtahun, attrs = {'class' : 'form-control-sm', 'style' : 'border: 1px solid #ced4da; background: white; width: 33.3%;'}),
    )
    waktu = forms.TimeField(
        label = 'Waktu', input_formats = ['%H:%M'],
        widget = forms.TextInput(
            attrs={'class' : 'form-control form-control-sm', 'placeholder' : '00:00'}),
    )
    kategori = forms.ChoiceField(
        label = 'Kategori',
        choices=[('Akademis', 'Akademis'),
                ('Organisasi/ Kepanitiaan', 'Organisasi/ Kepanitiaan'),
                ('Keluarga', 'Keluarga'),
                ('Teman', 'Teman'),
                ('Lainnya', 'Lainnya')],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm'})
    )
    
    
    class Meta:
        model = models.Sched
        fields = ('nama', 'tempat', 'tanggal', 'waktu', 'kategori')
